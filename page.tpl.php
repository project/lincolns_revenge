<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="<?php print $language ?>" xml:lang="<?php print $language ?>" xmlns="http://www.w3.org/1999/xhtml">
<head>
 <title><?php print $head_title ?></title>
 <?php print $head ?>
 <style type="text/css" media="screen">
  <!--
    @import url(<?php print path_to_theme()."/layout.css"; ?>);
    @import url(<?php print path_to_theme()."/modules.css"; ?>);
  // -->
  </style>
 <?php print $styles ?>
  <!--[if lte IE 6]>
  <style type="text/css" media="screen">
  <!--
    /* IE min-width trick */
    div#wrapper1 { 
      width:expression(((document.compatMode && document.compatMode=='CSS1Compat') ? document.documentElement.clientWidth : document.body.clientWidth) < 720 ? "720px" : "auto"); 
      }
  // -->
  </style>
  <![endif]-->
</head>

<body <?php print theme("onload_attribute"); ?>>
<div class="hide">
  <?php if ($site_slogan) : ?>
    <div id="site-slogan"><span><?php print($site_slogan) ?></span></div>
  <?php endif;?>
  <a href="#content" title="Skip the site navigation to go directly to the content">Skip to content</a>
</div>
<!-- closes #header-->
<!-- START: HEADER 
  |||||||||||||||||||||||||||||||||||||||||||||||||||||||| -->
<div id="wrapper1">
 <div id="wrapper2">
  <div class="header" id="header">
   <div class="headerpadding">
    <?php if ($logo) : ?>
     <div id="site-name"><a href="<?php print url() ?>" title="Home"><img
      src="<?php print $logo ?>" alt="<?php print $site_name ?> Logo" /></a></div>
    <?php endif; ?>

    <?php if (module_exist('banner')) : ?>
     <div id="banner"><?php print banner_display() ?></div>
    <?php endif; ?>

    <?php if ($primary_links) : ?>
     <ul id="primary">
      <?php foreach (array_reverse($primary_links) as $link): ?>
       <li><?php print $link; ?></li>
      <?php endforeach; ?>
     </ul>

    <?php elseif ($secondary_links) : ?>
     <ul id="secondary">
      <?php foreach (array_reverse($secondary_links) as $link): ?>
       <li><?php print $link; ?></li>
      <?php endforeach; ?>
     </ul>
    <?php endif; ?>
        <?php if ($search_box) { ?>
	 <form action="<?php print $search_url; ?>" method="post" id="search">
	  <div><input class="form-text" type="text" size="15" value="" name="edit[keys]" id="keys" />
	   <input class="form-submit" type="submit" id="submit" value="Search" /></div>
	 </form>
	<?php } ?>

      </div>
    </div>
<!-- END: HEADER
  |||||||||||||||||||||||||||||||||||||||||||||||||||||||| -->  

    <hr class="hide" />  

    <div class="columns">
      <div class="leftcolumn sidebar" id="sidebar-left">
        <div class="leftpadding">
          <?php if ($sidebar_left != '') { ?>
          <div class="sidebar" id="sidebar-left">
            <?php print $sidebar_left; ?>
          </div>
          <?php } ?>
        </div>
      </div>
      <div class="rightcolumn sidebar" id="sidebar-right">
        <div class="rightpadding">
          <?php if ($sidebar_right != '') { ?>
            <div class="sidebar" id="sidebar-right">
              <?php print $sidebar_right; ?>
            </div>
          <?php } ?>
        </div>
      </div>
      <div class="centercolumn">
        <div class="centerpadding">
				
          <div class="main-content" id="main">
            <?php if ($breadcrumb != ""): ?>
              <div id="breadcrumbs">
                <?php print $breadcrumb ?>
              </div>
            <?php endif; ?>
            <?php if ($mission != ""): ?>
              <div id="mission"><span><?php print $mission ?></span></div>
            <?php endif; ?>
            <?php if ($page_title != ""): ?>
              <h1 id="title"><?php print $page_title ?></h1>
            <?php endif; ?>
            <?php if ($message != ""): ?>
              <div id="message"><?php print $message ?></div>
            <?php endif; ?>
            <?php if ($help != ""): ?>
              <p id="help"><?php print $help ?></p>
            <?php endif; ?>
  
            <!-- start main content -->
            <?php print($content) ?>
            <!-- end main content -->
  
            
          </div><!-- main -->

        </div>
      </div>
    </div>

    <div class="clearing"></div>

<hr class="hide" />

<!-- START: FOOTER
  |||||||||||||||||||||||||||||||||||||||||||||||||||||||| -->
    <div id="footer" class="footer">
      <?php if ($footer_message) : ?>
        <p><?php print $footer_message;?></p>
      <?php endif; ?>
      <?php print $closure;?>
    </div>
<!-- END: FOOTER 
  |||||||||||||||||||||||||||||||||||||||||||||||||||||||| -->
		
	</div><!-- wrapper -->
</div><!-- outer_wrapper -->


</body>
</html>

