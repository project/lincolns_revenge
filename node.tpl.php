<div class="node<?php print ($static) ? " static" : ""; ?>">
  <?php if ($avatar): ?>
    <div class="avatar"><?php print $avatar ?></div>
  <?php endif; ?>

  <?php if ($page == 0): ?>
    <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
  <?php endif; ?>

  <?php if ($nodetype != 'page'): ?>
    <div class="info"><span class="submitted">Submitted by <?php print $name; ?> on <?php print $date; ?></span></div>
  <?php endif; ?>

  <?php if ($terms): ?>
    <div class="taxonomy"><?php print $terms; ?></div>
  <?php endif; ?>

  <div class="content">
   <?php print $content ?>
  </div>

  <?php if ($links): ?>
    <div class="links">&raquo; <?php print $links ?></div>
  <?php endif; ?>
</div>
  
