<div class="comment">
  <?php if ($avatar): ?>
    <div class="avatar"><?php print $avatar ?></div>
  <?php endif; ?>
  <h3 class="title"><?php print $title ?></h3>
  <div  class="submitted"><?php print t("Submitted by %a on %b.", array("%a" => $author, "%b" => $date)) ?>    <?php if ($new): ?>
  <span class="new"><?php print t("new") ?></span></div>

  <?php endif; ?>
  <div class="content">
    <?php print $content; ?>
  </div>

  <?php if ($links): ?>
    <div class="links">&raquo; <?php print $links ?></div>
  <?php endif; ?>
</div>
